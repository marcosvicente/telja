json.extract! sale, :id, :name, :price, :created_at, :updated_at
json.url sale_url(sale, format: :json)
