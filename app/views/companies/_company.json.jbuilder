json.extract! company, :id, :name, :cnpj, :phone, :email, :address, :city, :state, :country, :created_at, :updated_at
json.url company_url(company, format: :json)