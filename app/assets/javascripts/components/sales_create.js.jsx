var SalesCreate = React.createClass({

  handleSubmit(sales) {
    var newState = this.state.sales.concat(sales);
    this.setState({
      sales: newState
     })
   },
  render: function() {
    return (
      <div>
        <SalesNew handleSubmit={this.handleSubmit}/>
      </div>

    );
  }
});
