var SalesNew = React.createClass({
  propTypes: {
    sales: React.PropTypes.array
  },
  getInitialState: function() {
    return {name: '', price: '' };
  },
  handleNameChange: function(e) {
    this.setState({ name: e.target.value });
  },
  handlePriceChange: function(e) {
    this.setState({ price: e.target.value });
  },

  handleSubmit: function(e) {
    e.preventDefault();
    var name = this.state.name.trim();
    var price = this.state.price.trim();
    if (!name || !price ) {
      return;
    }
    this.setState({ name: '', price: ''});

    var sales =  {"sales": {"name": this.state.name, "price": this.state.price}};
    var data =  JSON.stringify(sales);
    console.log(data);
    // Submit form via jQuery/AJAX
    $.ajax({
      data: data,
      url: '/sales.json/',
      type: "POST",
      contentType: "application/json; charset=utf-8",
      cache: false,
      dataType: "json",
      success: (data) => {
        this.props.handleSubmit(data);
      }
    });

},



  render: function() {
    return (


    <div>
      <form onSubmit={ this.handleSubmit }>
        <div className="modal-content">
          <label>Digite nome</label>
          <input type="text"ref="name" value={ this.state.name } onChange={ this.handleNameChange } />

          <label>Digite seu preço</label>
          <input type="text"ref="price" value={ this.state.price } onChange={ this.handlePriceChange } />
        </div>
        <div className="modal-footer">
          <a className=" modal-action modal-close waves-effect waves-red red btn-flat">Fechar</a>
          <input type="submit" value="Post" className=" modal-action modal-close green waves-effect waves-green btn-flat"/>
        </div>
      </form>



      </div>


    )
  }
});
