var SalesList = React.createClass({



  render: function(){
    return (
        <table>
          <thead>
            <tr>
              <th>Nome</th>
              <th>Preço</th>
            </tr>
          </thead>
          <tbody>
              {
                this.props.sales.map(function(sales){
                  return(
                    <tr key={sales.id}>
                      <td>{sales.name} </td>
                      <td>{sales.price}</td>
                    </tr>
                  )
                }
              )}
          </tbody>
        </table>
    );
  }
});
