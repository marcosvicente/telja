var SalesAll = React.createClass({
  getInitialState: function(){
         return {
            sales: [],
            loading: true
      };
     },
     loading() {
       return this.state.loading ? 'loading-table' : '';
 	   },
     componentWillMount: function(){
        var self = this;
        setTimeout(function() {
         $.get('/sales.json/', function(sales){
            self.setState({
              loading: false,
              sales: sales
            })
         }.bind(this));
       }, 4000);
     },

     handleSubmit(sales) {
       var newState = this.state.sales.concat(sales);
       this.setState({
         sales: newState
        })
      },


     render: function(){
         return (
             <div className={this.loading()}>
                <div className="message-loading center">
                  <h2>Carregando ...</h2>
                    <div className="preloader-wrapper big active">
                      <div className="spinner-layer spinner-blue-only">
                        <div className="circle-clipper left">
                          <div className="circle">
                        </div>
                      </div>
                      <div className="gap-patch">
                        <div className="circle"></div>
                      </div>
                      <div className="circle-clipper right">
                      <div className="circle"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <SalesList sales={this.state.sales}   />

                <div id="modal" className="modal modal-fixed-footer">
                  <SalesNew handleSubmit={this.handleSubmit}/>

                </div>

             </div>
          );
     }

});
