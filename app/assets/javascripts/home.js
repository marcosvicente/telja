// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$( document ).ready(function(){

    $(".button-collapse").sideNav();
    //dropdown
    $('.dropdown-button').dropdown({
       inDuration: 300,
       outDuration: 225,
       constrain_width: false, // Does not change width of dropdown to that of the activator
       hover: true, // Activate on hover
       gutter: 0, // Spacing from edge
       belowOrigin: false, // Displays dropdown below the button
       alignment: 'rigth' // Displays dropdown with edge aligned to the left of button
    });
    $('.message-menu').dropdown({
       inDuration: 300,
       outDuration: 225,
       constrain_width: false, // Does not change width of dropdown to that of the activator
       hover: true, // Activate on hover
       gutter: ($('.dropdown-content').width()*3)/2.5 + 5, // Spacing from edge
       belowOrigin: true, // Displays dropdown below the button
       alignment: 'right' // Displays dropdown with edge aligned to the left of button
    });
    $('.notifications-menu').dropdown({
       inDuration: 300,
       outDuration: 225,
       constrain_width: false, // Does not change width of dropdown to that of the activator
       hover: true, // Activate on hover
       gutter: ($('.dropdown-content').width()*3)/2.5 + 5, // Spacing from edge
       belowOrigin: true, // Displays dropdown below the button
       alignment: 'right' // Displays dropdown with edge aligned to the left of button
    });
    $(".message-menu").sideNav();
    $( ".collapsible-header" ).click(function() {
        $(".more",this).toggle()
        $(".less", this).toggle()
    });



    $(".modal-trigger").leanModal();

    $("#search-on-click").click(function() {
        $(".message-card").toggle()
    });


});
