class ProductedsController < ApplicationController
  before_action :set_producted, only: [:show, :edit, :update, :destroy]

  # GET /producteds
  # GET /producteds.json
  def index
    @producteds = Producted.all
  end

  # GET /producteds/1
  # GET /producteds/1.json
  def show
  end

  # GET /producteds/new
  def new
    @producted = Producted.new
  end

  # GET /producteds/1/edit
  def edit
  end

  # POST /producteds
  # POST /producteds.json
  def create
    @producted = Producted.new(producted_params)

    respond_to do |format|
      if @producted.save
        format.html { redirect_to @producted, notice: 'Producted was successfully created.' }
        format.json { render :show, status: :created, location: @producted }
      else
        format.html { render :new }
        format.json { render json: @producted.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /producteds/1
  # PATCH/PUT /producteds/1.json
  def update
    respond_to do |format|
      if @producted.update(producted_params)
        format.html { redirect_to @producted, notice: 'Producted was successfully updated.' }
        format.json { render :show, status: :ok, location: @producted }
      else
        format.html { render :edit }
        format.json { render json: @producted.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /producteds/1
  # DELETE /producteds/1.json
  def destroy
    @producted.destroy
    respond_to do |format|
      format.html { redirect_to producteds_url, notice: 'Producted was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_producted
      @producted = Producted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def producted_params
      params.require(:producted).permit(:name, :price, :type, :amount, :validity, :category, :created, :modificated)
    end
end
