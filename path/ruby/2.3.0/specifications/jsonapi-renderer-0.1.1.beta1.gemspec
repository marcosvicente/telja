# -*- encoding: utf-8 -*-
# stub: jsonapi-renderer 0.1.1.beta1 ruby lib

Gem::Specification.new do |s|
  s.name = "jsonapi-renderer".freeze
  s.version = "0.1.1.beta1"

  s.required_rubygems_version = Gem::Requirement.new("> 1.3.1".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Lucas Hosseini".freeze]
  s.date = "2016-10-02"
  s.description = "Low-level renderer for JSONAPI documents.".freeze
  s.email = "lucas.hosseini@gmail.com".freeze
  s.homepage = "https://github.com/beauby/jsonapi".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.6.10".freeze
  s.summary = "Render JSONAPI documents.".freeze

  s.installed_by_version = "2.6.10" if s.respond_to? :installed_by_version
end
