module I18n
  module Locale
    autoload :Fallbacks, 'i18n/locale/fallbacks'
    autoload :Tag,       'i18n/locale/tag'

    I18n.load_path += Dir[Rails.root.join('lib', 'locale', '*.{rb,yml}')]
    I18n.default_locale = :pt
    I18n.available_locales = [:en, :pt]
  end
end
