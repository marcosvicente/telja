class CreateProducteds < ActiveRecord::Migration
  def change
      # Produted é uma tabela que mostrá todas os produtos cadastrado
    create_table :producteds do |t|
      t.string :name
      t.integer :price
      t.string :type
      t.string :amount
      t.date :validity
      t.integer :category
  

      t.timestamps null: false
    end
  end
end
