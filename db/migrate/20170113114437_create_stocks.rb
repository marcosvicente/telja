class CreateStocks < ActiveRecord::Migration
  def change
     # Stock e uma tabela que mostrará todos os produtos em estoque
    create_table :stocks do |t|
      t.string :amount
      t.string :validity
      

      t.timestamps null: false
    end
  end
end
