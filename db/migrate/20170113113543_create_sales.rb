class CreateSales < ActiveRecord::Migration
  def change
     # Sale e uma tabela que mostrará as vendas de todos os produtos
    create_table :sales do |t|
      t.string :name
      t.integer :price
      

      t.timestamps null: false
    end

  end
end
