class CreateCompanies < ActiveRecord::Migration
  def change

    # Company e uma tabela que terá informações sobre a compania
    create_table :companies do |t|
      t.string :name
      t.integer :cnpj
      t.integer :phone
      t.string :email
      t.string :address
      t.string :city
      t.string :state
      t.string :country

      t.timestamps null: false
    end
  end
end
