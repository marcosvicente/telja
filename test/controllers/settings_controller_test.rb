require 'test_helper'

class SettingsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get custom_account" do
    get :custom_account
    assert_response :success
  end

  test "should get change_password" do
    get :change_password
    assert_response :success
  end

end
