require 'test_helper'

class ProductedsControllerTest < ActionController::TestCase
  setup do
    @producted = producteds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:producteds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create producted" do
    assert_difference('Producted.count') do
      post :create, producted: { amount: @producted.amount, category: @producted.category, created: @producted.created, modificated: @producted.modificated, name: @producted.name, price: @producted.price, type: @producted.type, validity: @producted.validity }
    end

    assert_redirected_to producted_path(assigns(:producted))
  end

  test "should show producted" do
    get :show, id: @producted
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @producted
    assert_response :success
  end

  test "should update producted" do
    patch :update, id: @producted, producted: { amount: @producted.amount, category: @producted.category, created: @producted.created, modificated: @producted.modificated, name: @producted.name, price: @producted.price, type: @producted.type, validity: @producted.validity }
    assert_redirected_to producted_path(assigns(:producted))
  end

  test "should destroy producted" do
    assert_difference('Producted.count', -1) do
      delete :destroy, id: @producted
    end

    assert_redirected_to producteds_path
  end
end
